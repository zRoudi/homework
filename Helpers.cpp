#include "Helpers.h"

double dSum2(double& lParam, double& rParam)
{
    auto tempResult = lParam + rParam;
    return tempResult*tempResult;
}
