﻿
#include <iostream>
#include "Queue.h"
#include "Stack.h"
#include "Animals.h"

using namespace std;

int main()
{
	Queue<int> q;
	q.Push(1);
	q.Push(2);
	q.Push(3);
	cout << "Queue: " << q.Pop() << ' ' << q.Pop() << '\n';

	Stack<int> s;
	s.Push(1);
	s.Push(2);
	s.Push(3);
	cout << "Stack: " << s.Pop() << ' ' << s.Pop() << '\n';


	Animal* Array[3];
	Array[0] = new Cat;
	Array[1] = new Dog;
	Array[2] = new Horse;

	for (auto e : Array)
		e->Voice();

} 