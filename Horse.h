#pragma once
#include "Animal.h"
class Horse :
    public Animal
{
    void Voice() override {
        std::cout << "BRRR\n";
    }
};

