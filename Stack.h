#pragma once

template<class type>
class Stack
{
private:
	static const int SizeIncrement = 16;
	int Size = 0;
	type* Array = nullptr;
	int CurrentElement = 0;
public:

	Stack() {
		Array = new type[Size];
	};

	Stack(const type InitParam, const int _Size) :Size(_Size) {
		Array = new type[Size];
		for (type& element : Array)
			element = InitParam;
		CurrentElement = Size;
	}

	~Stack() {
		delete[] Array;
	}

	type Pop();

	void Push(const type param);

	void Resize();
};

template<class type>
inline type Stack<type>::Pop()
{
	if (CurrentElement > 0) {
		return Array[--CurrentElement];
	}
}

template<class type>
inline void Stack<type>::Push(const type param)
{
	if (CurrentElement == Size) {
		Resize();
	}
	Array[CurrentElement++] = param;
}

template<class type>
inline void Stack<type>::Resize()
{
	type* TempArray = Array;
	Array = new type[Size + SizeIncrement];
	for (register int i = 0; i < Size; i++) {
		Array[i] = TempArray[i];
	}
	delete[] TempArray;
	Size += SizeIncrement;
}