#pragma once


template<class type>
class Queue
{
private:
	static const int SizeIncrement = 16;
	int Size = 0;
	type* Array = nullptr;
	int CurrentElement = 0, LastElement = 0;
public:

	Queue() {
		Array = new type[Size];
	};

	Queue(const type InitParam, const int _Size):Size(_Size) {
		Array = new type[Size];
		for (type& element : Array)
			element = InitParam;
		LastElement = Size;
	}

	~Queue() {
		delete[] Array;
	}

	type Pop();

	void Push(const type param);

	void Resize();
};




template<class type>
inline type Queue<type>::Pop()
{
	if (CurrentElement < LastElement) {
		return Array[CurrentElement++];
	}
}

template<class type>
inline void Queue<type>::Push(const type param)
{
	if (LastElement == Size) {
		Resize();
	}
	Array[LastElement++] = param;
}

template<class type>
inline void Queue<type>::Resize()
{
	type* TempArray = Array;
	Array = new type[Size + SizeIncrement];
	for (register int i = 0; i < Size; i++) {
		Array[i] = TempArray[i];
	}
	delete[] TempArray;
	Size += SizeIncrement;
}
